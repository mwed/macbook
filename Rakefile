namespace :ssh do
  desc 'setup ssh'
  task :setup do
    sh 'test -f ~/.ssh/id_rsa' do |ok, _status|
      break if ok
      sh 'ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa -P ""'
    end
  end
end

namespace :docker do
  task :ecr do
    sh '$(aws ecr get-login --no-include-email --region ap-northeast-1)'
  end

  task :es_data do
    sh 'test -d /private/log/es_data' do |ok, _status|
      break if ok
      sh 'sudo mkdir -p /private/log/es_data'
    end
  end

  task :compose_up do
    sh 'docker-compose up --build -d'
  end

  desc 'setup docker'
  task setup: [:ecr, :es_data, :compose_up]
end

namespace :rbenv do
  task :bash_profile do
    sh 'cp bash_profile ~/.bash_profile'
  end

  task :gemrc do
    sh 'cp gemrc ~/.gemrc'
  end

  task :install_ruby do
    sh 'rbenv install 2.4.4'
  end

  task :global do
    sh 'rbenv global 2.4.4'
  end

  task :bundler do
    sh 'gem install bundler'
  end

  namespace :bundle do
    task :install do
      sh 'bundle install'
    end
  end

  desc 'setup rbenv'
  task setup: [:bash_profile, :gemrc, :install_ruby, :global, :bundler, "bundle:install"]
end

namespace :brew do
  task :install do
    sh 'which brew' do |ok, _status|
      break if ok
      sh '/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"'
    end
  end

  task :bundle do
    sh 'brew bundle'
  end

  task :start do
    sh 'brew services start mysql@5.6'
  end

  desc 'setup homebrew'
  task setup: [:install, :bundle, :start]
end
