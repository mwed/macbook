source "https://rubygems.org"
gem "rails", "5.2.2"

gem "aasm"
gem "action_args"
gem "actionmailer_inline_css"
gem "active_decorator"
gem "activerecord-import"
gem "activerecord5-redshift-adapter"
gem "aws-sdk"
gem "bcrypt"
gem "bootstrap3-datetimepicker-rails"
gem "cocoon"
gem "coffee-rails", "~> 4.2"
gem "config"
gem "dalli"
gem "dhash-vips"
gem "doorkeeper"
gem "elasticsearch-model"
gem "elasticsearch-persistence"
gem "elasticsearch-rails"
gem "exifr"
gem "fastimage"
gem "fluent-logger"
gem "font-awesome-sass", "4.7.0"
gem "geocoder"
gem "haml-rails"
gem "high_voltage"
gem "holiday_jp", git: "https://github.com/holiday-jp/holiday_jp-ruby", ref: "7ba1b6d"
gem "html-pipeline"
gem "httparty"
gem "jbuilder"
gem "jquery-rails"
gem "jquery-ui-rails"
gem "json", ">2"
gem "kaminari"
gem "listen"
gem "loofah", ">= 2.2.1"
gem "mime-types", require: false
gem "momentjs-rails"
gem "mysql2", "0.4.10"
gem "omniauth-google-oauth2"
gem "parallel"
gem "payjp"
gem "pg", "0.21.0"
gem "puma", "3.10.0"
gem "rack-health"
gem "rack-user_agent"
gem "rake"
gem "redcarpet"
gem "redshift_cursor"
gem "reverse_markdown"
gem "revision_plate", require: "revision_plate/rails"
gem "safe_yaml", require: false
gem "sass-rails"
gem "savon"
gem "scss_lint", require: false
gem "sendgrid-ruby"
gem "sentry-raven"
gem "sidekiq"
gem "slack-notifier"
gem "split", require: "split/dashboard"
gem "sprockets-es6"
gem "sprockets-rails"
gem "switch_point"
gem "uglifier"
gem "webpacker"
gem "yaml_vault"

group :development do
  gem "bullet"
  gem "fluentd"
  gem "meta_request"
  gem "rack-mini-profiler", require: false
  gem "view_source_map"
  gem "web-console"
end

group :development, :staging do
  gem "letter_opener_web"
  gem "ruby-progressbar"
end

group :development, :test do
  gem "bootsnap", require: false
  gem "brakeman", require: false
  gem "elasticsearch-extensions"
  gem "factory_bot_rails"
  gem "foreman"
  gem "haml_lint", require: false
  gem "knapsack_pro"
  gem "onkcop", "~> 0.53", require: false
  gem "parser"
  gem "pronto", "~> 0.9.5", require: false
  gem "pronto-brakeman", require: false
  gem "pronto-haml", require: false
  gem "pronto-rails_best_practices", require: false
  gem "pronto-reek", require: false
  gem "pronto-rubocop", require: false
  gem "pry-byebug"
  gem "pry-doc", require: false
  gem "pry-rails"
  gem "rails_best_practices", require: false
  gem "reek", require: false
  gem "rubocop", require: false
  gem "synvert"
end

group :test do
  gem "active_decorator-rspec", require: false
  gem "capybara", "~> 2.18"
  gem "capybara-selenium"
  gem "chromedriver-helper"
  gem "coveralls", require: false
  gem "database_rewinder"
  gem "fakefs", require: "fakefs/safe"
  gem "launchy"
  gem "poltergeist"
  gem "rails-controller-testing", require: false
  gem "rspec-kickstarter", require: false
  gem "rspec-rails"
  gem "rspec-retry"
  gem "rspec_junit_formatter"
  gem "selenium-webdriver"
  gem "shoulda-matchers", "~> 3.1"
  gem "simplecov", require: false
  gem "vcr"
  gem "webmock"
end

group :production, :development do
  gem "newrelic_rpm"
end
gem "rails-html-sanitizer", ">= 1.0.4"
